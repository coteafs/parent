## v-2.0.0 - 14/07/2018

* Migration from GitHub to GitLab.
* Dependencies Update
  * maven-javadoc: 3.0.0 -> 3.0.1
  * maven-surefire: 2.20.1 -> 2.22.0
  * maven-clean: 3.0.0 -> 3.1.0
  * maven-resource: 3.0.2 -> 3.1.0
  * jacoco: 0.8.0 -> 0.8.1
  * sonar: 5.1.1.13214 -> 5.5.0.14655
  * guava: 23.0 -> 25.1-jre
  * testng: 6.14.2 -> 6.14.3
  * truth: 0.37 -> 0.41
  * snake-yaml: 1.19 -> 1.21